resource "aws_ecr_repository" "ecr_repo" {
    name                 = "vmetrix-app"
    image_tag_mutability = "MUTABLE"
    image_scanning_configuration {
        scan_on_push = false
    }
}
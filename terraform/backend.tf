terraform {
    backend "s3" {
        bucket         = "terraform-state-vmetrix-aws"   # Reemplaza con el nombre de tu bucket
        key            = "poc_vmetrix/terraform.tfstate"         # Path al archivo de estado en el bucket
        region         = "us-east-1"                     # Región del bucket S3
        dynamodb_table = "vmetrix-terraform-lock"        # para el bloqueo de estado
        # encrypt        = true                          # Opcional: para encriptar el archivo de estado
    }
}